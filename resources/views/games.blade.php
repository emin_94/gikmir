@extends('layouts.main')

@section('seo')
    <title>База игр | GIKMIR</title>
    <meta name="description" content="База игр GIKMIR">
    <meta name="description" content="База игр GIKMIR">
    <meta name="og:description" content="База игр GIKMIR">
    <meta name="twitter_title" content="База игр GIKMIR">
    <meta name="twitter_description" content="База игр GIKMIR">
    <meta name="author" content="GIKMIR">
    <meta name="site_name" content="GIKMIR">
    <meta name="image" content="//favicons/android-icon-192x192.png">
    <meta name="og:image" content="//favicons/android-icon-192x192.png">
    <meta name="og:type" content="article">
    <meta name="og:url" content="//gikmir.ru">
@endsection

@section('content')

    <!-- Page top section -->
    <section class="page-top-section set-bg" data-setbg="/img/page-top-bg/1.jpg">
        <div class="page-info">
            <h2>База Игр</h2>
            <div class="site-breadcrumb">
                <a href="/">Главная</a>  /
                <span>База Игр</span>
            </div>
        </div>
    </section>
    <!-- Page top end-->


    <!-- Games section -->
    <section class="games-section">
        <div class="container">
            {{--<ul class="game-filter">
                <li><a href="">A</a></li>
                <li><a href="">B</a></li>
                <li><a href="">C</a></li>
                <li><a href="">D</a></li>
                <li><a href="">E</a></li>
                <li><a href="">F</a></li>
                <li><a href="">G</a></li>
                <li><a href="">H</a></li>
                <li><a href="">I</a></li>
                <li><a href="">J</a></li>
                <li><a href="">K</a></li>
                <li><a href="">L</a></li>
                <li><a href="">M</a></li>
                <li><a href="">N</a></li>
                <li><a href="">O</a></li>
                <li><a href="">P</a></li>
                <li><a href="">Q</a></li>
                <li><a href="">R</a></li>
                <li><a href="">S</a></li>
                <li><a href="">T</a></li>
                <li><a href="">U</a></li>
                <li><a href="">V</a></li>
                <li><a href="">W</a></li>
                <li><a href="">X</a></li>
                <li><a href="">Y</a></li>
                <li><a href="">Z</a></li>
            </ul>--}}
            <div class="row">
                <games-component></games-component>
                <div class="col-xl-3 col-lg-4 col-md-5 sidebar game-page-sideber">
                    <div id="stickySidebar">
                        <search-widget-component></search-widget-component>
                        @include('widgets.simple', ['title' => 'Жанры' , 'data' => $genres, 'type' => 'genres'])
                        @include('widgets.simple', ['title' => 'Платформы' , 'data' => $platforms, 'type' => 'platforms'])
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Games end-->

@endsection
