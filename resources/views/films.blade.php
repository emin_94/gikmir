@extends('layouts.main')

@section('seo')
    <title>Новости Кино | GIKMIR</title>
    <meta name="description" content="Последние и самые интересные новосьти мира кинофильмов на сайте GIKMIR">
    <meta name="description" content="Последние и самые интересные новосьти мира кинофильмов на сайте GIKMIR">
    <meta name="og:description" content="Последние и самые интересные новосьти мира кинофильмов на сайте GIKMIR">
    <meta name="twitter_title" content="Новости Кино">
    <meta name="twitter_description" content="Последние и самые интересные новосьти мира кинофильмов на сайте GIKMIR">
    <meta name="author" content="GIKMIR">
    <meta name="site_name" content="GIKMIR">
    <meta name="image" content="//favicons/android-icon-192x192.png">
    <meta name="og:image" content="//favicons/android-icon-192x192.png">
    <meta name="og:type" content="article">
    <meta name="og:url" content="//gikmir.ru/film-news">
@endsection

@section('content')
    <!-- Page top section -->
    <section class="page-top-section set-bg" data-setbg="/img/page-top-bg/1.jpg">
        <div class="page-info">
            <h1>Новости Кино</h1>
        </div>
    </section>
    <!-- Page top end-->

    <!-- Blog section -->
    @include('sections.posts', [$posts, 'type' => ['game', 'technology', 'film']])
    <!-- Blog section end -->

@endsection
