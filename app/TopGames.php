<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class TopGames extends Model implements HasMedia
{
    use InteractsWithMedia;

    public function registerAllMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('slide')
            ->crop('crop-center', 279, 390);
    }
}
