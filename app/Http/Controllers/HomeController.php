<?php

namespace App\Http\Controllers;

use App\Post;
use App\Slider;
use App\TopGames;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $slider = Slider::with('media')->latest()->get();
        $top_games = TopGames::with('media')->latest()->get();
        $posts = Post::with('media')->latest()->paginate(12);

        return view('home', compact('slider', 'posts', 'top_games'));
    }
}
