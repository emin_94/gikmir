<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumsForParsingToPublishersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $column = Schema::hasColumn('publishers', 'slug');
        $column2 = Schema::hasColumn('publishers', 'page');
        $column3 = Schema::hasColumn('publishers', 'key');
        Schema::table('publishers', function (Blueprint $table) use ($column, $column2, $column3) {
            if (!$column){
                $table->string('slug')->unique();
            }
            if (!$column2){
                $table->string('page');
            }
            if (!$column3){
                $table->unsignedInteger('key')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publishers', function (Blueprint $table) {
            $table->dropColumn('page');
            $table->dropColumn('key');
        });
    }
}
