<div class="widget-item">
    <h4 class="widget-title">Тренды</h4>
    <div class="trending-widget">
        @foreach ($games as $game)
            <div class="tw-item">
                <div class="tw-thumb">
                    <img src="{{ $game->firstImage('images', 'small-thumb') !== '' ? $game->firstImage('images', 'small-thumb') : $game->background_image }}" alt="{{ $game->title }}">
                </div>
                <div class="tw-text">
                    <div class="tw-meta">{{ $game->release_date->translatedFormat('d F Y') }}</div>
                    <h5><a href="/games/{{ $game->slug }}/{{$game->id }}">{{ Str::limit($game->title, 26) }}</a></h5>
                </div>
            </div>
        @endforeach
    </div>
</div>
