<section class="blog-section spad">
    <div class="container">
        <div class="row">
            <div class="col-xl-9 col-lg-8 col-md-7">
                <div class="section-title text-white">
                    <h2>Последние новости</h2>
                </div>
{{--                <ul class="blog-filter">--}}
{{--                    <li><a href="#">Racing</a></li>--}}
{{--                    <li><a href="#">Shooters</a></li>--}}
{{--                    <li><a href="#">Strategy</a></li>--}}
{{--                    <li><a href="#">Online</a></li>--}}
{{--                </ul>--}}
                <!-- Blog item -->
                @foreach($posts as $post)
                <div class="blog-item animated" data-mouse-scrool="fade-up" >
                    <div class="blog-thumb lazy-bg border-circle" onmouseover="hoverAction(this)" onclick="hoverAction(this)" onmouseleave="leaveAction(this)">
                        {{--Video--}}
                        @if ($post->youtube_id)
                            {{--If we have video. Video on hover--}}
                            <iframe src=""
                                    data-url="https://www.youtube-nocookie.com/embed/{{$post->youtube_id}}?controls=0&amp;autoplay=1&start=30&loop=1&playlist={{$post->youtube_id}}&mute=1"
                                    frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                            </iframe>
                            {{--If we have video. Single image--}}
                            <img class="lazy" src="/img/lazy.jpg" data-original="{{ $post->firstImage('images', 'thumb') }}" alt="{{ $post->title }}">
                            <span class="video-play-icon"></span>
                                @else
                            {{--If we dont have video. Show all images--}}
                            @include('components.post-thumb-gallery', ['images' => $post->allImages('images', 'thumb')])
                        @endif
                    </div>
                    <div class="blog-text text-box text-white">
                        <div class="top-meta">
                            {{ $post->created_at->diffForHumans() }}
                            <h3><a href="/{{$type[$post->type]}}-news/{{ $post->slug }}">{{ Str::limit($post->title, 40) }}</a></h3>
                            <p>{{ Str::limit($post->description, 350) }}</p>
                            <a href="/{{$type[$post->type]}}-news/{{ $post->slug }}" class="read-more">Читать дальше <img src="/img/icons/double-arrow.png" alt="{{ $post->title }}"/></a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="col-xl-3 col-lg-4 col-md-5 sidebar">
                <div id="stickySidebar">
                    {{--Trands widget--}}
                    @empty(!$widgets->find(1))
                        @include('widgets.tranding', ['games' => $widgets->find(1)->games])
                    @endempty
{{--                    Categories widget--}}
{{--                    <div class="widget-item">--}}
{{--                        <div class="categories-widget">--}}
{{--                            <h4 class="widget-title">categories</h4>--}}
{{--                            <ul>--}}
{{--                                <li><a href="">Games</a></li>--}}
{{--                                <li><a href="">Gaming Tips & Tricks</a></li>--}}
{{--                                <li><a href="">Online Games</a></li>--}}
{{--                                <li><a href="">Team Games</a></li>--}}
{{--                                <li><a href="">Community</a></li>--}}
{{--                                <li><a href="">Uncategorized</a></li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    Add widget--}}
                    <div class="widget-item">
                        <a href="https://store.playstation.com/en-us/product/UP0002-CUSA19035_00-CB4STANDARD00001" target="_blank" ref="nofollow noopener" class="add">
                            <img class="lazy" src="/img/lazy_16.jpg" data-original="/img/crash_banner.webp" alt="add name">
                        </a>
                    </div>
{{--                    <div class="widget-item">--}}
{{--                        <a href="#" ref="nofollow noopener" class="add">--}}
{{--                            <img src="./img/add.jpg" alt="add name">--}}
{{--                        </a>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="paginate">
                {{ $posts->links() }}
            </div>
        </div>
    </div>
</section>
