<section class="hero-section overflow-hidden">
    <div class="hero-slider owl-carousel">
            @forelse($slider as $slide)
                <div class="hero-item set-bg d-flex align-items-center justify-content-center text-center owl-lazy" style="background: #b01ba5;" data-src="{{$slide->firstImage('image', 'slide')}}">
                    <div class="container">
                        <h2>{{$slide->title}}</h2>
                        <p>{{$slide->description}}</p>
                        @if($slide->slug)
                            <a href="{{$slide->slug}}" class="site-btn">Читать далее  <img src="/img/icons/double-arrow.png" alt="{{$slide->title}}"/></a>
                        @endif
                    </div>
                </div>
                    @empty
                        <div class="hero-item set-bg d-flex align-items-center justify-content-center text-center owl-lazy" data-src="img/slider-bg-1.jpg">
                            <div class="container">
                                <h2>Game on!</h2>
                                <p>Fusce erat dui, venenatis et erat in, vulputate dignissim lacus. Donec vitae tempus dolor,<br>sit amet elementum lorem. Ut cursus tempor turpis.</p>
                                <a href="#" class="site-btn">Read More  <img src="/img/icons/double-arrow.png" alt="#"/></a>
                            </div>
                        </div>
                        <div class="hero-item set-bg d-flex align-items-center justify-content-center text-center owl-lazy" data-src="img/slider-bg-2.jpg">
                            <div class="container">
                                <h2>Game on!</h2>
                                <p>Fusce erat dui, venenatis et erat in, vulputate dignissim lacus. Donec vitae tempus dolor,<br>sit amet elementum lorem. Ut cursus tempor turpis.</p>
                                <a href="#" class="site-btn">Read More  <img src="/img/icons/double-arrow.png" alt="#"/></a>
                            </div>
                        </div>
            @endforelse
    </div>
</section>
