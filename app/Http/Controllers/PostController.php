<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function gameNews()
    {
        $posts = Post::with('media')->whereStatus(1)->latest()->where('type', 0)->paginate(15);
        return view('game-news', compact('posts'));
    }

    public function gameNewsSingle($slug)
    {
//        Написать query !!!! на завтра
        $post = Post::with(
            [
                'media',
                'games' => function ($query) {$query->with('media');}
            ]
        )->whereStatus(1)->where('slug', $slug)->firstOrFail();
        return view('post-single', compact('post'));
    }

    public function films()
    {
        $posts = Post::with('media')->whereStatus(1)->latest()->where('type', 2)->paginate(15);
        return view('films', compact('posts'));
    }

    public function technologies()
    {
        $posts = Post::with('media')->whereStatus(1)->latest()->where('type', 1)->paginate(15);
        return view('technologies', compact('posts'));
    }
}
