@extends('layouts.main')

@section('seo')
    <title>Игоровые новости | GIKMIR</title>
    <meta name="description" content="Здесь вы найдете последние игровые новости, только самое интересное на сайте GIKMIR">
    <meta name="description" content="Здесь вы найдете последние игровые новости, только самое интересное на сайте GIKMIR">
    <meta name="og:description" content="Здесь вы найдете последние игровые новости, только самое интересное на сайте GIKMIR">
    <meta name="twitter_title" content="Игоровые новости">
    <meta name="twitter_description" content="Здесь вы найдете последние игровые новости, только самое интересное на сайте GIKMIR">
    <meta name="author" content="GIKMIR">
    <meta name="site_name" content="GIKMIR">
    <meta name="image" content="//favicons/android-icon-192x192.png">
    <meta name="og:image" content="//favicons/android-icon-192x192.png">
    <meta name="og:type" content="article">
    <meta name="og:url" content="//gikmir.ru/game-news">
@endsection

@section('content')
    <!-- Page top section -->
    <section class="page-top-section set-bg" data-setbg="/img/page-top-bg/1.jpg">
        <div class="page-info">
            <h1>Игоровые новости</h1>
        </div>
    </section>
    <!-- Page top end-->
    <!-- Blog section -->
    @include('sections.posts',  [$posts, 'type' => ['game', 'technology', 'film']])
    <!-- Blog section end -->
@endsection
