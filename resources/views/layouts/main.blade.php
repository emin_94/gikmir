<!DOCTYPE html>
<html lang="ru">

@include('partials.head')

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KFKL4R8"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    @yield('additionalTopScript') {{--For Adding script from page--}}

    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    @include('partials.header')

    <div id="app">
        @yield('content')
    </div>

    <script src="{{ asset('js/app.js') }}"></script>

    @include('partials.footer_js')

    @yield('additionalBottomScript') {{--For Adding script from page--}}
    @include('partials.footer')

</body>

</html>
