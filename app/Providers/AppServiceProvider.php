<?php

namespace App\Providers;

use App\Group;
use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Builder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Builder::defaultStringLength(191);
        Paginator::defaultView('vendor.pagination.default');
//      ----------------- For all pages
        $widgets = Group::with('games')->get();
        view()->share(['widgets' => $widgets, 'version' => '0.7.3']);
    }
}
