@extends('layouts.main')

@section('seo')
    <title>GIKMIR | Личный кабинет</title>
@endsection

@section('content')

    <!-- Page top section -->
    <section class="page-top-section set-bg" data-setbg="/img/page-top-bg/1.jpg">
        <div class="page-info">
            <h1>Личный кабинет</h1>
        </div>
    </section>
    <!-- Page top end-->

    <div class="my-cab">

        <cab-action-component></cab-action-component>


        <!-- my posts section -->
{{--        @include('sections.my-posts', ['data' => $data ?? null])--}}
        <!-- my posts section end-->
    </div>


@endsection
