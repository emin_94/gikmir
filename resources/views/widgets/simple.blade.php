@if(!empty($data->first()))
    <div class="widget-item">
        <div class="categories-widget">
            <h4 class="widget-title">{{$title}}</h4>
            <ul>
                @foreach($data as $item)
                    <li><a href="/find/games/{{$type}}/{{$item->id}}">{{$item->name}}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
