<!--====== Javascripts & Jquery ======-->
<script src="/js/jquery-3.2.1.min.js"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/jquery.lazyload.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js.map"></script>
<script src="/js/jquery.slicknav.min.js"></script>
{{--<script src="/js/jquery.sticky-sidebar.min.js"></script>--}}
<script src="/js/jquery.magnific-popup.min.js"></script>
<script src="/js/main.js"></script>
