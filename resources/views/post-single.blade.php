@extends('layouts.main')

@section('seo')
    <title>{{$post->title}} | GIKMIR</title>
    <meta name="description" content="{{Str::limit($post->description, 120)}}">
    <meta name="og:description" content="{{Str::limit($post->description, 120)}}">
    <meta name="twitter_title" content="{{$post->title}}">
    <meta name="twitter_description" content="{{Str::limit($post->description, 120)}}">
    <meta name="author" content="GIKMIR">
    <meta name="site_name" content="GIKMIR">
    <meta name="image" content="{{$post->firstImage('images', 'thumb')}}">
    <meta name="og:image" content="{{$post->firstImage('images', 'thumb')}}">
    <meta name="og:type" content="article">
    <meta name="og:url" content="//gikmir.ru/game-news/{{$post->slug}}">
@endsection

@section('additionalTopScript')
    <script>
        function share_fb(url) {
            console.log(url)
            window.open('https://www.facebook.com/sharer/sharer.php?u='+url,'facebook-share-dialog',"width=626, height=436")
        }
    </script>

@endsection

@section('content')

    <!-- Page top section -->
    <section class="page-top-section set-bg" data-setbg="/img/page-top-bg/1.jpg">
        <div class="page-info">
            <h2>{{$post->title}}</h2>
            <div class="site-breadcrumb">
                @if ($post->type === 0)
                    <a href="/">Главная</a>  /
                    <a href="/game-news">Игоровые новости</a> /
                @endif
                @if ($post->type === 1)
                    <a href="/">Главная</a>  /
                    <a href="/technology-news">Технологии</a> /
                @endif
                @if ($post->type === 2)
                    <a href="/">Главная</a>  /
                    <a href="/film-news">Новости фильмов</a> /
                @endif
                <span>{{$post->title}}</span>
            </div>
        </div>
    </section>
    <!-- Page top end-->


    <!-- Games section -->
    <section class="games-single-page">
        <div class="container">
            <div class="game-single-preview">
                <div class="post-thumb-gallery owl-carousel">
                    @foreach($post->getMedia('images') as $image)
                        <a href="{{$image->getUrl()}}" class="popup-link">
                            <img class="owl-lazy" data-src="{{$image->getUrl('main')}}" alt="{{$post->title}}" />
                        </a>
                    @endforeach
                </div>
            </div>
            <div class="row">
                <div class="col-xl-9 col-lg-8 col-md-7 game-single-content">
                    <div class="gs-meta">{{$post->created_at->format('d.m.Y')}}</div>
                    <h1 class="gs-title">{{$post->title}}</h1>
                    {{--Контент--}}
                    {!! $post->text !!}
                    <div class="geme-social-share pt-5 d-flex">
                        <p>Поделиться:</p>
                        <a onclick="share_fb('https://gikmir.ru/game-news/{{$post->slug}}');return false;" rel="nofollow" share_url="https://gikmir.ru/game-news/{{$post->slug}}" target="_blank">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a target="_blank" rel="nofollow" href='https://twitter.com/intent/tweet?text=https://bestar.bestcreative.az/tours/{{$post->slug}}'>
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a target="_blank" rel="nofollow" href='//api.whatsapp.com/send?phone=whatsappphonenumber&text=https://gikmir.ru/game-news/{{$post->slug}}'>
                            <i class="fa fa-whatsapp"></i>
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-5 sidebar game-page-sideber">
                    <div id="stickySidebar">
                    @empty(!$post->video)
                            @include('widgets.video')
                    @endempty

                    @empty(!$widgets->find(1))
                            @include('widgets.tranding', ['games' => $widgets->find(1)->games])
                    @endempty
{{--                        <div class="widget-item">--}}
{{--                            <div class="rating-widget">--}}
{{--                                <h4 class="widget-title">Ratings</h4>--}}
{{--                                <ul>--}}
{{--                                    <li>Price<span>3.5/5</span></li>--}}
{{--                                    <li>Graphics<span>4.5/5</span></li>--}}
{{--                                    <li>Levels<span>3.5/5</span></li>--}}
{{--                                    <li>Levels<span>4.5/5</span></li>--}}
{{--                                    <li>Dificulty<span>4.5/5</span></li>--}}
{{--                                </ul>--}}
{{--                                <div class="rating">--}}
{{--                                    <h5><i>Rating</i><span>4.5</span> / 5</h5>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="widget-item">--}}
{{--                            <div class="testimonials-widget">--}}
{{--                                <h4 class="widget-title">Testimonials</h4>--}}
{{--                                <div class="testim-text">--}}
{{--                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolo re magna aliqua. Quis ipsum suspend isse ultrices.</p>--}}
{{--                                    <h6><span>James Smith,</span>Gamer</h6>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Games end-->

{{--    <section class="game-author-section">--}}
{{--        <div class="container">--}}
{{--            <div class="game-author-pic set-bg" data-setbg="/img/author.jpg"></div>--}}
{{--            <div class="game-author-info">--}}
{{--                <h4>Written by: Michael Williams</h4>--}}
{{--                <p>Vivamus volutpat nibh ac sollicitudin imperdiet. Donec scelerisque lorem sodales odio ultricies, nec rhoncus ex lobortis. Vivamus tincid-unt sit amet sem id varius. Donec elementum aliquet tortor. Curabitur justo mi, efficitur sed eros alique.</p>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}


    <!-- Newsletter section -->
    @include('sections.newslatter')
    <!-- Newsletter section end -->

@endsection
