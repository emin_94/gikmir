<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParsingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parsings', function (Blueprint $table) {
            $table->id();

            $table->string('title');
            $table->text('text')->nullable();

            $table->json('images')->nullable();
            $table->json('options')->nullable();

            $table->string('platform')->nullable();
            $table->string('series')->nullable();
            $table->string('publisher')->nullable();
            $table->string('genre')->nullable();

            $table->string('release_date')->nullable();
            $table->string('announce_date')->nullable();

            $table->string('url')->unique();
            $table->string('page_number')->nullable();
            $table->dateTime('is_parsed')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parsings');
    }
}
