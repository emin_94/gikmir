<div class="widget-item">
    <h4 class="widget-title">Видео</h4>
    <div class="trending-widget">
        <div class="tv-item">
            <a href="#youtube-popup" class="open-popup-link">
                <img src="{{$post->firstImage('images', 'thumb')}}" alt="{{$post->title}}">
                <span class="video-play-icon"></span>
            </a>
        </div>
    </div>
</div>

{{--YouTube POPUP--}}
<div id="youtube-popup" class="page-popup mfp-hide">
    <iframe src="https://www.youtube-nocookie.com/embed/{{$post->youtube_id}}?loop=1&playlist={{$post->youtube_id}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
