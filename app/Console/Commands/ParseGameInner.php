<?php

namespace App\Console\Commands;

use App\Parsing;
use Illuminate\Console\Command;
use App\Traits\ParsingTrait;

class ParseGameInner extends Command
{
    use ParsingTrait;

    protected $signature = 'parse:game_inners';
    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $ParsingClass = new Parsing;
        $parsing_urls = $ParsingClass->orderBy('id', 'DESC')->select('url')->where('is_parsed', null)->pluck('url');

        foreach($parsing_urls as $key => $url) {
            $this->report('_________________________________', 'white');

            $this->report([$key, $url], 'green');
            $html = $this->getPage($url);
            $text_tag = 'body > div.outer_outer > div > div.wrapper_outer > div > div > div > div.main-block > div.game-content > div.description';

            $text = $this->getHtmlTag($html, $text_tag);
            $genres = $this->getHtmlTag($html, '.game-tags .tag');

            $optionKeys = $this->getHtmlTag($html, '.game-data .title');
            $optionValues = $this->getDinamHtmlTag($html, '.game-data .value');
            $options = $this->makeObjectKeyValue($optionKeys, $optionValues);

            echo "\n * Genres__________";
            $this->report($genres);
            echo "\n\n * Text____________ \n";
            $this->report($text);
            echo "\n\n * Options____________ \n";
            $this->report($options);
        };

        $this->report('Поздравляю парсинг закончен !!!', 'green');
        return true;
    }
}
