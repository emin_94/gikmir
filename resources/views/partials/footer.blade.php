<footer class="footer-section">
    <div class="container">
        <div class="footer-left-pic">
            <img src="/img/footer-left-pic.png" alt="left pic">
        </div>
        <div class="footer-right-pic">
            <img src="/img/footer-right-pic.png" alt="right pic">
        </div>
        <a href="/" class="footer-logo">
            <img src="/img/logo.png" alt="gikmir logo">
        </a>
        <ul class="main-menu footer-menu">
            <li><a href="/">Главная</a></li>
            <li><a href="/game-news">Игровые новости</a></li>
            <li><a href="/film-news">Фильмы</a></li>
            <li><a href="/technology-news">Технологии</a></li>
            <li><a href="/games">База игр</a></li>
        </ul>
        <br>
{{--        <div class="footer-social d-flex justify-content-center">--}}
{{--            <a href="#"><i class="fa fa-facebook"></i></a>--}}
{{--            <a href="#"><i class="fa fa-instagram"></i></a>--}}
{{--        </div>--}}
        <div class="copyright">2020 All rights reserved /  v{{ $version }}</div>
    </div>
    <div class="bestsite"><a href="https://bestsite.az" target="_blank"><img src="/img/bestsite-games.png" alt="bestsite-games"></a></div>
</footer>
