@extends('layouts.main')

@section('seo')
    <title>{{$game->title}} | GIKMIR</title>
    <meta name="description" content="Информация о игре {{$game->title}}">
    <meta name="description" content="Информация о игре {{$game->title}}">
    <meta name="og:description" content="Информация о игре {{$game->title}}">
    <meta name="twitter_title" content="{{$game->title}} | GIKMIR">
    <meta name="twitter_description" content="Информация о игре {{$game->title}}">
    <meta name="author" content="GIKMIR">
    <meta name="site_name" content="GIKMIR">
    <meta name="image" content="{{$game->title}}">
    <meta name="og:image" content="{{$game->title}}">
    <meta name="og:type" content="article">
    <meta name="og:url" content="https://gikmir.ru/games/{{$game->slug}}/{{$game->id}}">
@endsection

@section('content')

    <!-- Page top section -->
    <section class="page-top-section set-bg" data-setbg="/img/page-top-bg/1.jpg">
        <div class="page-info">
            <h2>{{$game->title}}</h2>
            <div class="site-breadcrumb">
                <a href="/">Главная</a>  /
                <a href="/games">Игры</a>  /
                <span>{{$game->title}}</span>
            </div>
        </div>
    </section>
    <!-- Page top end-->

    <!-- Games section -->
    <section class="games-single-page">
        <div class="container">
            <div class="game-single-preview">
                <img src="{{$game->background_image}}" alt="{{$game->title}}">
            </div>
            <div class="row">
                <div class="col-xl-9 col-lg-8 col-md-7 game-single-content">
                    <div class="gs-meta">Дата релиза / {{$game->release_date ? $game->release_date->format('d.m.Y') : 'Неизвестна'}}</div>
                    <h1 class="gs-title">{{$game->title}}</h1>
                    {!! $game->description !!}
                    <div class="geme-social-share pt-5 d-flex">
                        <p>Поделиться:</p>
                        <a href="#"><i class="fa fa-pinterest"></i></a>
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-dribbble"></i></a>
                        <a href="#"><i class="fa fa-behance"></i></a>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-5 sidebar game-page-sideber">
                    <div id="stickySidebar">
                        <div class="widget-item">
                            <div class="rating-widget">
                                <h4 class="widget-title">Рейтинг игры</h4>
                                <div class="rating">
                                    <h5><i>Rating</i><span>{{$game->rating}}</span></h5>
                                </div>
                            </div>
                        </div>
                        @include('widgets.simple', ['title' => 'Жанры' , 'data' => $game->genres, 'type' => 'genres'])
                        @include('widgets.simple', ['title' => 'Платформы' , 'data' => $game->platforms, 'type' => 'platforms'])
                        @include('widgets.simple', ['title' => 'Серии' , 'data' => $game->series, 'type' => 'series'])
                        @include('widgets.simple', ['title' => 'Издатель' , 'data' => $game->publishers, 'type' => 'publishers'])
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Games end-->
{{--    {{dd($game)}}--}}

@endsection
