<?php

namespace App\Console\Commands;

use App\Game;
use App\Genre;
use App\Platform;
use App\Publisher;
use App\Traits\ParsingTrait;
use Illuminate\Console\Command;
use Carbon\Carbon;

class ParseAgRuGames extends Command
{
    use ParsingTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:ag_games';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        echo "\n Начало Парсинга: \n";
        info("Парсинг запущен " . Carbon::now('Asia/Baku') );

        $this->init();
    }

    public function init()
    {
        $this->parseGames();
    }

    protected function parseGames($next = null)
    {
        $last = (new Game)->latest()->first();

        if ($next) {
            $page = $next;
        } else if ($last && $last->page) {
            $page = $last->page;
        } else {
            $page = "https://api.ag.ru/api/games?comments=true&filter=true&page=1&page_size=40";
        }
        $this->report('url is = ' . $page);
        $data = $this->api($page);
        echo "\n\n";
        foreach ($data['results'] as $key => $value) {
            $ParsingModel = new Game;

            if ($ParsingModel->where('slug', $value['slug'])->count()) {
                $this->report("Повтор ! " . $value['name'], 'yellow');
            }
            else {
                $game_url = 'https://ag.ru/api/games/' . $value['slug'];
                echo "\n";
                $this->report($value['name'].' Parsing... url ='.$game_url, 'green');
                $this->parseGameInner($game_url, $key, $page);
            }
        }
        //------------------- next page ...
        if ($data['next']) {
            $this->report('Next Page is ' . $data['next'], 'blue');
            sleep(0.3);
            $this->parseGames($data['next']);
        } else {
            $this->report('parse Games DONE !!!', 'green');
            return true;
        }
    }

    public function parseGameInner ($url, $key, $page) {
        $data = $this->api($url);

        $ParsingModel = new Game;
        $ParsingModel->title = $data['name'];
        $ParsingModel->description = $data['description'];
        $ParsingModel->release_date = $data['released'];
        $ParsingModel->rating = $data['rating'];
        $ParsingModel->metacritic = $data['metacritic'];
        $ParsingModel->background_image = $data['background_image'];
        $ParsingModel->slug = $data['slug'];
        $ParsingModel->parse_page = $url;
        $ParsingModel->key = $key;
        $ParsingModel->page = $page;

        if ($ParsingModel->save()) {
            $this->report($key . " - " . $data['name'] . ': saved');
            // save Genres
            if ($data['genres']) {
                foreach ($data['genres'] as $item) {
                    $item = Genre::where('name', $item['name'])->first();
                    $ParsingModel->genres()->attach($item);
                    if ($item) $this->report($key . " - Genre = " . $item['name'] . ' saved');
                }
            }
            // save Platforms
            if ($data['platforms']) {
                foreach ($data['platforms'] as $item) {
                    $item = $item['platform'];
                    $item = Platform::where('name', $item['name'])->first();
                    $ParsingModel->platforms()->attach($item);
                    if ($item) $this->report($key . " - Platform = " . $item['name'] . ' saved');
                }
            }
            // save Platforms
            if ($data['publishers']) {
                foreach ($data['publishers'] as $item) {
                    if ($item['name']) {
                        $item = Publisher::where('name', $item['name'])->first();
                        $ParsingModel->publishers()->attach($item);
                        if ($item) $this->report($key . " - Publisher = " . $item['name'] . ' saved');
                    }
                }
            }
        }
        else {
            $this->report($key . " - " . 'error', 'red');
        }
    }


    public function api($url)
    {
        $cURLConnection = curl_init();
        curl_setopt($cURLConnection, CURLOPT_URL, $url);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
        $phoneList = curl_exec($cURLConnection);
        curl_close($cURLConnection);

        return $data = json_decode($phoneList, true);
    }
}
