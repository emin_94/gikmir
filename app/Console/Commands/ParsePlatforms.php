<?php

namespace App\Console\Commands;

use App\Platform;
use Illuminate\Console\Command;

class ParsePlatforms extends Command
{
    protected $signature = 'parse:platforms';
    protected $description = 'Command description';

    // Variables ***
        public $pageNumber;
        public $pageUrl;
    // Variables ###

    public function __construct()
    {
        parent::__construct();
    }

    public function init()
    {
        $ParsingModel = new Platform;

            $this->pageNumber = 1;
            $this->pageUrl = "https://www.igromania.ru/games/platforms/";

    }

    public function handle()
    {
        $this->init();
        echo "\n Начало Парсинка: \n \e[32m Страница: " . $this->pageNumber . " \e[0m " . $this->pageUrl . " \n";

        info("Парсинг запущен ". now() );
        $this->getGameNameAndUrl($this->pageUrl);
    }

    public function getGameNameAndUrl($url)
    {
        try {
            $html = file_get_contents($url);
        }catch (\Exception $e) {
            // Если ошибка перезапускаем
            echo $e;
            info($e);
            $this->handle();
        }

        $html = file_get_contents($url);
        $page = \phpQuery::newDocument($html);

        $gameTitles = $page->find('div.games-content > p > a');

        foreach ($gameTitles as $key => $value) {
            $pqLink = pq($value); //pq делает объект phpQuery
            $name = $pqLink->html();

            $ParsingModel = new Platform;
            if ($ParsingModel->where('name', $name)->count() === 0 && iconv_strlen($name) < 150) {
                $ParsingModel->name = $name;
                $ParsingModel->save();
                echo "Страница " . $this->pageNumber . ") " . ($key + 1) . ". " . "name = " . $name . "\n";
            }
            else{
                echo "\e[33m Страница " . $this->pageNumber . ") || " . ($key + 1) . ". " . " Повтор !!" . "\e[0m \n";
            }
        }
        return $this->getNextPage($page, 'body > div.outer_outer > div > div.wrapper_outer > div > div > div > div.main-block > div.games-content > div > a');
    }

    public function getNextPage($page, $tag)
    {
        $pagesLinks = $page->find($tag);
        foreach ($pagesLinks as $link) {
            $pqLink = pq($link); //pq делает объект phpQuery
            if ($pqLink->html() === 'далее'){
                // Запускаем getGameNameAndUrl с адресом следующей страницы
                $this->pageNumber = str_replace(['/games/platforms/', '/'],'',$pqLink->attr('href'));
                echo "\n \e[32m Следующая страница $this->pageNumber " . 'https://www.igromania.ru' . $pqLink->attr('href') . " \e[0m \n \n";

                sleep(0.5);
                return $this->getGameNameAndUrl('https://www.igromania.ru' . $pqLink->attr('href'));
            }
        }
        // Если следующая страница не найдена, заканчиваем парсинг
        echo "\n \e[32m Парсинг Закончен !!! \e[0m \n";
        return false;
    }
}
