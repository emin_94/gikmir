<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "HomeController@index")->name('home');

// Games
Route::get('/game-news', "PostController@gameNews")->name('game-news');
Route::get('/game-news/{slug}', "PostController@gameNewsSingle")->name('game-news-single');

// Games Data
Route::get('/games', 'GameController@games');
Route::get('/all-games', 'GameController@allGames');
Route::get('/games-by-type/{type}/{id}', 'GameController@gamesByType');
Route::get('/games/{slug}/{game}', 'GameController@singleGame');
Route::get('/find/games/{type}/{id}', 'GameController@games');
Route::get('/search/game/{param}', 'GameController@searchGame');

// Technologies
Route::get('/technology-news/{slug}', "PostController@gameNewsSingle")->name('game-news-single');
Route::get('/technology-news', "PostController@technologies")->name('technologies');
Route::get('/technologies', function () { return redirect('technology-news', 301); });

// Films
Route::get('/film-news', "PostController@films")->name('kino-news');
Route::get('/film-news/{slug}', "PostController@gameNewsSingle")->name('game-news-single');
Route::get('/kino-news', function () { return redirect('film/news', 301); });

// Cabinet
Route::get('/my-cab', 'MyController@index');

// Contact
Route::get('/contact', function () {
    return view('blog-h');
});


Route::get('/test', function () {
    return view('contact');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
