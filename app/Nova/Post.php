<?php

namespace App\Nova;

use Carbon\Carbon;
use Ebess\AdvancedNovaMediaLibrary\Fields\Media;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Boolean;
use NovaAjaxSelect\AjaxSelect;
use Spatie\Image\Image;

class Post extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Post::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Media::make('Images')->rules('required'),

            Text::make('Title')->rules('required'),
            Text::make('URL', 'slug')->rules('unique')->rules('required'),
            Textarea::make('Description')->rules('required'),
            Trix::make('Text')->rules('required'),

            Select::make('Type')
                ->options(['Games', 'Technologies', 'Films']),

            BelongsTo::make('Games')->searchable()->nullable(),

            Boolean::make('Status')->default(0),
            DateTime::make('Start At')->firstDayOfWeek('1')->default(Carbon::now())->format("Y-m-d H:mm:ss"),
            DateTime::make('Created At')->firstDayOfWeek('1')->default(Carbon::now())->format("Y-m-d H:mm:ss")->sortable(),

            Text::make('Video')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
