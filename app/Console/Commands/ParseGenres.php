<?php

namespace App\Console\Commands;

use App\Genre;
use App\Traits\ParsingTrait;
use Illuminate\Console\Command;

class ParseGenres extends Command
{
    use ParsingTrait;

    protected $signature = 'parse:genres';
    protected $description = 'Command description';

    // Variables ***


    // Variables ###

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->init();
    }

    public function init()
    {
        $url = "https://ag.ru/api/genres";

       $data = $this->api($url);
       echo "\n\n";
       foreach ($data['results'] as $key => $genre) {
           $ParsingModel = new Genre;
           $ParsingModel->name = $genre['name'];
           if ($ParsingModel->save()) {
               echo $key . " - " . $this->report('saved', 'green');
           } else {
               echo $key . " - " . $this->report('error', 'red');
           }
       }
    }

    function api($url) {
        // create & initialize a curl session
        $cURLConnection = curl_init();

        curl_setopt($cURLConnection, CURLOPT_URL, $url);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

        $phoneList = curl_exec($cURLConnection);
        curl_close($cURLConnection);

        return $data = json_decode($phoneList, true);
    }
}
