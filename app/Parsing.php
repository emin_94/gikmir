<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parsing extends Model
{
    protected function allCount() {
        $this->count();
    }
    protected function count() {
        $this->where('is_parsed', null)->count();
    }
}
