<?php

namespace App\Http\Controllers;

use App\Game;
use App\Genre;
use App\Platform;
use App\Publisher;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    public function games()
    {
        $platforms = Platform::limit(5)->get();
        $genres = Genre::limit(5)->get();
        return view('games', compact('genres', 'platforms'));
    }

    public function allGames(Game $games)
    {
        $games = $games->with('media')
            ->select('id', 'title', 'description', 'rating', 'slug', 'background_image')
            ->simplePaginate(24);
        return \Response::json($games, 200);
    }

    public function gamesByType($type, $id)
    {
        $games = Game::whereHas($type, function ($q) use ($id) {
            $q->where('id', $id);
        })->simplePaginate(24);

        return \Response::json($games, 200);
    }

    public function singleGame($slug, $id)
    {
        $game = Game::with('media', 'genres', 'platforms', 'series', 'publishers', 'posts')->findOrFail($id);
        if (!($game->slug === $slug)) return abort(404);
        return view('game-single', compact('game'));
    }

    public function searchGame($param)
    {
        return Game::where('title', 'LIKE', $param.'%')
            ->select('id', 'title', 'slug')
            ->orderBy('title')
            ->limit(25)->get();
    }
}
