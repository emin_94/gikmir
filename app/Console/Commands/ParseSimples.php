<?php

namespace App\Console\Commands;

use App\Genre;
use App\Platform;
use App\Publisher;
use App\Traits\ParsingTrait;
use http\Url;
use Illuminate\Console\Command;

class ParseSimples extends Command
{
    use ParsingTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:simples';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        info('Parsing Simples Started !');

//        $this->report("parseGenres START", "red");
//        $this->parseGenres();
//        $this->report("parsePlatforms START", "red");
//        $this->parsePlatforms();
        $this->report("parsePublishers START", "red");
        $this->parsePublishers();

        echo "\n";
        $this->report('||*****|| Парсинг завершен ! ||*****||', 'green');
        return 0;
    }

    public function parseGenres()
    {
        $url = "https://ag.ru/api/genres";
        $this->report('url is = ' . $url);
        $data = $this->api($url);
        echo "\n\n";
        foreach ($data['results'] as $key => $genre) {
            $ParsingModel = new Genre;
            $ParsingModel->name = $genre['name'];
            if ($ParsingModel->save()) {
                echo $key . " - " . $this->report('saved');
            } else {
                echo $key . " - " . $this->report('error', 'red');
            }
        }
        $this->report('parseGenres DONE' . 'green');
    }
    public function parsePlatforms($next = null)
    {
        if ($next) {$url = $next;}
        else {$url = "https://api.ag.ru/api/platforms?page=1&page_size=40";}
        $this->report('url is = ' . $url);

        $data = $this->api($url);
        echo "\n\n";
        foreach ($data['results'] as $key => $genre) {
            $ParsingModel = new Platform;
            $ParsingModel->name = $genre['name'];
            if ($ParsingModel->save()) {
                echo $key . " - " . $this->report('saved', 'green');
            } else {
                echo $key . " - " . $this->report('error', 'red');
            }
        }
        if ($data['next']) {
            $this->report('Next Page is '.$data['next'], 'blue');
            $this->parsePlatforms($data['next']);
        } else {
            $this->report('parsePlatforms id DONE !!!', 'white');
            return true;
        }
    }
    public function parsePublishers($next = null)
    {
        $last = (new Publisher)->latest()->first();

        if ($next) {$url = $next;}
        else if ($last && $last->page) {
            $url = $last->page;
        }
        else {$url = "https://ag.ru/api/publishers?page=1&page_size=40";}
        $this->report('url is = ' . $url);
        $data = $this->api($url);
        echo "\n\n";
        foreach ($data['results'] as $key => $value) {
            $ParsingModel = new Publisher;

            if ($ParsingModel->where('slug', $value['slug'])->count()) {
                $this->report("Повтор ! " . $value['name'], 'yellow');
            }
            else {
                $ParsingModel->name = $value['name'];
                $ParsingModel->slug = $value['slug'];
                $ParsingModel->page = $url;
                $ParsingModel->key = $key;

                if ($ParsingModel->save()) {
                    $this->report($key . " - " . 'saved', 'white');
                } else {
                    $this->report($key . " - " . 'error', 'red');
                }
            }
        }
        //-------------------
        if ($data['next']) {
            $this->report('Next Page is '.$data['next'], 'blue');
            sleep(0.3);
            $this->parsePublishers($data['next']);
        } else {
            $this->report('parse Publishers DONE !!!', 'green');
            return true;
        }
    }

    function api($url) {
        // create & initialize a curl session
        $cURLConnection = curl_init();
        curl_setopt($cURLConnection, CURLOPT_URL, $url);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
        $phoneList = curl_exec($cURLConnection);
        curl_close($cURLConnection);

        return $data = json_decode($phoneList, true);
    }
}
