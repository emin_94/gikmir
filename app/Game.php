<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Game extends Model implements HasMedia
{
    use InteractsWithMedia, SoftDeletes;

    protected $dates = [
        'release_date',
        'announce_date'
    ];

    // firstImage and allImages

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->crop('crop-center', 130, 130);
        $this->addMediaConversion('small-thumb')
            ->crop('crop-center', 80, 80);
    }

    public function platforms()
    {
        return $this->belongsToMany(Platform::class);
    }

    public function genres()
    {
        return $this->belongsToMany(Genre::class);
    }

    public function series()
    {
        return $this->belongsToMany(Series::class);
    }

    public function publishers()
    {
        return $this->belongsToMany(Publisher::class);
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class);
    }
    public function posts()
    {
        return $this->HasMany(Post::class);
    }
}
