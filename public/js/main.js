/* =================================
------------------------------------
	EndGam - Gaming Magazine Template
	Version: 1.0
 ------------------------------------
 ====================================*/


'use strict';


$(window).on('load', function() {
	/*------------------
		Preloder
	--------------------*/
	$(".loader").fadeOut();
	$("#preloder").delay(400).fadeOut("slow");
    // addClassOnScrool();
    addClassWhenElementOnSeen();
});

(function($) {
	/*------------------
		Navigation
	--------------------*/
	$('.primary-menu').slicknav({
		appendTo:'.header-warp',
		closedSymbol: '<i class="fa fa-angle-down"></i>',
		openedSymbol: '<i class="fa fa-angle-up"></i>'
	});


	/*------------------
		Background Set
	--------------------*/
	$('.set-bg').each(function() {
		var bg = $(this).data('setbg');
		$(this).css('background-image', 'url(' + bg + ')');
	});



	/*------------------
		Hero Slider
	--------------------*/
	$('.hero-slider').owlCarousel({
		loop: true,
		nav: true,
		dots: true,
		navText: ['', '<img src="img/icons/solid-right-arrow.png">'],
		mouseDrag: false,
		animateOut: 'fadeOut',
		animateIn: 'fadeIn',
		items: 1,
		autoplay: true,
        lazyLoad : true,
        lazyLoadEager: 1,
        autoplayTimeout: 10000,
	});

	var dot = $('.hero-slider .owl-dot');
	dot.each(function() {
		var index = $(this).index() + 1;
		if(index < 10){
			$(this).html('0').append(index + '.');
		}else{
			$(this).html(index + '.');
		}
	});



	/*------------------
		Video Popup
	--------------------*/
	$('.video-popup').magnificPopup({
  		type: 'iframe'
	});

    $('#top-posts-slider').owlCarousel({
        center: false,
        items:1,
        loop:true,
        lazyLoad : true,
        lazyLoadEager: 4,
        margin:20,
        autoplay: false,
        autoplayHoverPause:true,
        autoplayTimeout: 4000,
        responsive:{
            1300: {
                items:5
            },
            1000: {
                items:4
            },
            875: {
                autoplay: true,
                items:3.4
            },
            768: {
                autoplay: true,
                items:3
            },
            600: {
                items:2.5
            },
            500: {
                items:2
            },
            450: {
                items:1.7
            },
            400: {
                items:1.5
            },
            300: {
                items:1.3,
                center: false
            }
        }
    });

    $(".post-thumb-gallery").owlCarousel({
        items : 1,
        lazyLoad : true,
        lazyLoadEager: 2,
        navigation : false,
        nav: true,
        margin: 5
    });


//    On Scroll

    $(window).scroll(function() {
        // addClassOnScrool();
        addClassWhenElementOnSeen();
    });

    // Lazi loading
    $(function() {
        $("img.lazy").lazyload({
            threshold : 200,
            effect : "fadeIn"
        });
    });

})($);

function addClassOnScrool() {
    let position = $(window).scrollTop(); // Позиция Экрана
    let attributes = $("[data-mouse-scrool]"); // Находим все элементы с атрибутом data-mouse-scrool
    for (let i = 0; i < Object.keys(attributes).length; i++) {
        let attribute = $(attributes[i]); // Берем элементы по отдельности
        if (attribute.offset()) {
            // Определяем позицию экоана для добавления класса
            let customPos = -60;
            let itemPos = attribute.offset().top - $(window).height() + customPos;
            if (!attribute.hasClass(attribute.data('mouseScrool')) && position > itemPos) {
                attribute.addClass(attribute.data('mouseScrool')); // Добавляем datа атрибута в класс
                attribute.addClass('activate'); // Добавляем нужные для нас классы
            }
        }
    }
}

function addClassWhenElementOnSeen() {
    let position = window.pageYOffset ; // Позиция Экрана
    let elements = document.querySelectorAll('[data-mouse-scrool]');
    for (let element of elements) {
        let elementPos = element.getBoundingClientRect().top;
        let attr = element.getAttribute('data-mouse-scrool');
        let earlier = 350; // Чтобы появился раньше
        if (!hasClassName(element.className, attr) && elementPos < parseInt(window.innerHeight) + earlier) {
            element.classList.add(attr); // Добавляем datа атрибута в класс
            element.classList.add('activate'); // Добавляем нужные для нас классы
        }
    }
}
function hasClassName(str, className) {
    let arr = str.split(' ');
    for(let ar of arr) { if (ar === className) return true }
    return false
}

function hoverAction(e) {
    let elem = $(e).find('iframe');
    elem.attr({src: elem.data('url')});
}
function leaveAction(e) {
    let elem = $(e).find('iframe');
    elem.attr({src: ''});
}

// Post dots hover action
$('.post-thumb-gallery .owl-dots .owl-dot').hover(
    function () { $(this).click() },
    () => {}
);

// Popup image
$('.popup-link').magnificPopup({
    gallery: {
        enabled: true,
        preload: [0,2],
        navigateByImgClick: true,
        arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>', // markup of an arrow button
        tPrev: 'Previous (Left arrow key)', // title for left button
        tNext: 'Next (Right arrow key)', // title for right button
        tCounter: '<span class="mfp-counter">%curr% из %total%</span>' // markup of counter
    },
    type: 'image',
    zoom: {
        enabled: true
    }
});
$('.open-popup-link').magnificPopup({
    type:'inline',
    midClick: true // Allow opening popup on middle mouse click.
});
