<?php


namespace App\Traits;

use phpQuery;

trait ParsingTrait {

    protected $url;
    protected $parseTag;
    protected $nextPageTag;
    protected $page;

    protected function parse(String $url, String $parseTag, $nextPageTag = '')
    {
        $this->url = $url;
        $this->parseTag = $parseTag;
        $this->nextPageTag = $nextPageTag;
        $this->getPage();

        return $this->getHtmlTag();
    }

    public function getHtmlTag($html, $parseTag, $single = false)
    {
        $gameTitles = $html->find($parseTag);
        $resp = [];
        foreach ($gameTitles as $value) {
            $pqLink = pq($value); //pq делает объект phpQuery
            if ($single) return $pqLink->html();
            else $resp[] = $pqLink->html();
        }
        return $resp;
    }

    public function getDinamHtmlTag($html, $parseTag, $single = false)
    {
        $gameTitles = $html->find($parseTag);
        $resp = [];
        foreach ($gameTitles as $value) {
            $pqLink = pq($value); //pq делает объект phpQuery
            $innerTag = pq($pqLink->find('a'))->html();
            if ($innerTag) {
                $resp[] = $innerTag;
            }
            else $resp[] = $pqLink->html();
        }
        return $resp;
    }

    protected function getPage($url)
    {
        try {
            $html = file_get_contents($url);
        } catch (\Exception $e) {
            // Если ошибка перезапускаем
            $this->report("file_get_contents('$url')" . "Не удалось загрузить HTML страницы", 'red');
            die();
        }
        return $this->page = phpQuery::newDocument($html);
    }

    protected function report($message, $color = "deff", $whatIs = false)
    {
        $colors = ["deff" => "\e[0m", "red" => "\e[31m", "white" => "\e[30m", "green" => "\e[32m", "yellow" => "\e[33m", "blue" => "\e[34m"];

        if (is_object($message) || is_array($message)){
            if ($whatIs) {echo "Object";}
            foreach ($message as $item){
                $report  = "\n" . $colors[$color];
                $report .= $item;
                $report .= $colors['deff'];
                echo ($report);
            }
        }
        else if (is_string($message)){
            if ($whatIs) {$this->report("String", 'blue');}
            $report  = "\n" . $colors[$color];
            $report .= $message;
            $report .= $colors['deff'];
            echo ($report);
        }
        else {
            $this->report(['Incorrect Value', 'Value must be array or string'], 'red');
        }
    }

    protected function makeObjectKeyValue(array $keys, array $values)
    {
        $arr = [];
        foreach ($keys as $i => $key) {
            $arr[] = [$key => $values[$i]];
        }
        return $arr;
    }


}
