<!-- Header section -->
<header class="header-section">
    <div class="header-warp">
{{--        <div class="header-social d-flex justify-content-end">--}}
{{--            <p>Follow us:</p>--}}
{{--            <a href="#"><i class="fa fa-facebook"></i></a>--}}
{{--            <a href="#"><i class="fa fa-instagram"></i></a>--}}
{{--        </div>--}}
        <div class="header-bar-warp d-flex">
            <!-- site logo -->
            <a href="/" class="site-logo">
                <img src="/img/logo.png" alt="GIKMIR">
            </a>
            <nav class="top-nav-area w-100">
                <!-- Menu -->
                <ul class="main-menu primary-menu">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/game-news">Игровые новости</a></li>
                    <li><a href="/film-news">Кино</a></li>
                    <li><a href="/technology-news">Технологии</a></li>
                    <li><a href="/games">База игр</a></li>

                    {{--@guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="/my-cab">Кабинет</a>

                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest--}}
                </ul>
            </nav>
        </div>
    </div>
</header>
<!-- Header section end -->
