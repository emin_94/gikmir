<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeGamesTableForParsing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->text('description')->nullable()->change();
            $table->string('parse_page')->after('description')->nullable();
            $table->double('rating', 3)->after('parse_page')->nullable();
            $table->double('metacritic')->after('rating')->nullable();
            $table->string('key')->nullable();
            $table->string('background_image', 255)->after('description')->nullable();
            $table->string('page')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->dropColumn('parse_page');
            $table->dropColumn('key');
            $table->dropColumn('rating');
            $table->dropColumn('metacritic');
            $table->dropColumn('page');
        });
    }
}
