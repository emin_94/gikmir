<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Series extends Model
{
    public function games()
    {
        return $this->belongsToMany(Game::class);
    }
}
