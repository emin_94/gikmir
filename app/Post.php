<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Post extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $dates = ['start_at' , 'created_at'];

    public function getYoutubeIdAttribute()
    {
        if ($this->video) {
            preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $this->video, $match);
            return $youtube_id = $match[1];
        }
        else return false;
    }

    public function registerAllMediaConversions(): void
    {
        $this->addMediaConversion('thumb')
            ->crop('crop-center', 510, 487);

        $this->addMediaConversion('main')
            ->crop('crop-center', 1149, 487);
    }

    public function games()
    {
        return $this->belongsTo(Game::class, 'game_id');
    }
}
