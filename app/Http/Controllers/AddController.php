<?php

namespace App\Http\Controllers;

use App\Add;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AddController extends Controller
{
    public function addUpload(Request $request)
    {
        return $request->file;

        $validator = Validator::make($request->all(), [
            'file' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        } else {

            $add = new Add;
            $add->addMedia($request->file)->toMediaCollection('image');;
            $add->save();
            return $add;
        }
    }
}
