@extends('layouts.main')

@section('seo')
    <title>GIKMIR | Новости мира игр , кино и технологий</title>
    <meta name="description" content="Последние новости мира игр, кино и технологий. Лучшее на сайте GIKMIR">
    <meta name="description" content="Последние новости мира игр, кино и технологий. Лучшее на сайте GIKMIR">
    <meta name="og:description" content="Последние новости мира игр, кино и технологий. Лучшее на сайте GIKMIR">
    <meta name="twitter_title" content="GIKMIR | Новости мира игр , кино и технологий">
    <meta name="twitter_description" content="Последние новости мира игр, кино и технологий. Лучшее на сайте GIKMIR">
    <meta name="author" content="GIKMIR">
    <meta name="site_name" content="GIKMIR">
    <meta name="image" content="//favicons/android-icon-192x192.png">
    <meta name="og:image" content="//favicons/android-icon-192x192.png">
    <meta name="og:type" content="article">
    <meta name="og:url" content="//gikmir.ru">
@endsection

@section('content')

    <!-- Hero section -->
    @include('sections.slider')
    <!-- Hero section end-->

    <!-- Intro section -->
    @include('sections.top-posts', $top_games)
    <!-- Intro section end -->

    <!-- Blog section -->
    @include('sections.posts', [$posts, 'type' => ['game', 'technology', 'film']])
    <!-- Blog section end -->

    <!-- Newsletter section -->

    <!-- Newsletter section end -->

@endsection
