<section class="intro-section">
    <div class="container max-w">
        <div class="row">
            <div id="top-posts-slider" class="top-games owl-carousel owl-theme">
                @foreach($top_games as $game)
                    <div class="intro-text-box text-box text-white card-bg-view owl-lazy" data-src="{{$game->firstImage('images')}}">
                        <div class="content">
                            <div class="top-meta">{{$game->updated_at->format('d.m.Y')}}</div>
                            <h3>{{$game->title}}</h3>
                            <p>{{$game->description}}</p>
                            @if (!empty($game->slug))
                                <a href="{{$game->slug}}" class="read-more">Читать далее<img style="display: inline; width: 10px" src="/img/icons/double-arrow.png" alt="{{$game->description}}"/></a>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
