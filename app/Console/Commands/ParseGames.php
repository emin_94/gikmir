<?php

namespace App\Console\Commands;

use App\Parsing;
use Illuminate\Console\Command;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseGames extends Command
{
//    Variables ***
    public $pageNumber;
    public $pageUrl;

    protected $signature = 'parse:games';
    protected $description = 'Command description';
//    Variables ###

    public function __construct()
    {
        parent::__construct();
    }

    public function init()
    {
        $ParsingModel = new Parsing;
        $ParsingModel->latest()->first();

        if ($ParsingModel->latest()->first()){
            $this->pageNumber = $ParsingModel->latest()->first()->page_number;
            $this->pageUrl = "https://www.igromania.ru/games/all/all/all/all/all/0/" . $this->pageNumber . "/";
        }else {
            $this->pageNumber = 1;
            $this->pageUrl = "https://www.igromania.ru/games/";
        }
    }

    public function handle()
    {
        $this->init();
        echo "\n Начало Парсинка: \n \e[32m Страница: " . $this->pageNumber . " \e[0m " . $this->pageUrl . " \n";

        info("Парсинг запущен ". now() );
        $this->getGameNameAndUrl($this->pageUrl);
    }

    public function getGameNameAndUrl($url)
    {

        try {
            $html = file_get_contents($url);
        }catch (\Exception $e) {
            // Если ошибка перезапускаем
            echo $e;
            info($e);
            $this->handle();
        }


        $html = file_get_contents($url);
        $page = \phpQuery::newDocument($html);

        $gameTitles = $page->find('.game-card .top-block .left-block a.name');

        foreach ($gameTitles as $key => $value) {
            $pqLink = pq($value); //pq делает объект phpQuery
            $title = $pqLink->html();
            $url = 'https://www.igromania.ru' . $pqLink->attr('href');

            $ParsingModel = new Parsing;
            if ($ParsingModel->where('url', $url)->count() === 0 && iconv_strlen($title) < 150 && iconv_strlen($url) < 150) {
                $ParsingModel->title = $title;
                $ParsingModel->url = $url;
                $ParsingModel->page_number = $this->pageNumber;
                $ParsingModel->save();
                echo "Страница " . $this->pageNumber . ") " . ($key + 1) . ". " . "title = " . $title . " url = " . $url . "\n";
            }
            else{
                echo "\e[33m Страница " . $this->pageNumber . ") || " . ($key + 1) . ". " . " Повтор URL = " . $url . "\e[0m \n";
            }
        }
        return $this->getNextPage($page, 'body > div.outer_outer > div > div.wrapper_outer > div > div > div > div.main-block > div.games-content.calendar > div.pages > a');
    }

    public function getNextPage($page, $tag)
    {
        $pagesLinks = $page->find($tag);
        foreach ($pagesLinks as $link) {
            $pqLink = pq($link); //pq делает объект phpQuery
            if ($pqLink->html() === 'далее'){
                // Запускаем getGameNameAndUrl с адресом следующей страницы
                $this->pageNumber = str_replace(['/games/all/all/all/all/all/0/', '/'],'',$pqLink->attr('href'));
                echo "\n \e[32m Следующая страница $this->pageNumber " . 'https://www.igromania.ru' . $pqLink->attr('href') . " \e[0m \n \n";

                sleep(0.5);
                return $this->getGameNameAndUrl('https://www.igromania.ru' . $pqLink->attr('href'));
            }
        }
        // Если следующая страница не найдена, заканчиваем парсинг
        echo "\n \e[32m Парсинг Закончен !!! \e[0m \n";
        return false;
    }
}
