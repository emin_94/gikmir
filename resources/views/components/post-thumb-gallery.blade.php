<div class="post-thumb-gallery owl-carousel">
    @foreach($images as $image)
        <div class="item"><img class="owl-lazy" data-src="{{ $image }}" alt=""></div>
    @endforeach
</div>
