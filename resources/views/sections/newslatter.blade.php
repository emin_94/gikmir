<section class="newsletter-section">
    <div class="container">
        <h2>Подпишитесь на наши обновления</h2>
        <form class="newsletter-form">
            <input type="text" placeholder="Ввидите E-mail">
            <button class="site-btn">Подписаться  <img src="/img/icons/double-arrow.png" alt="подписаться на | GIKMIR"/></button>
        </form>
    </div>
</section>
