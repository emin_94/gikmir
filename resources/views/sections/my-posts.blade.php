<section class="my-posts-section">
    <div class="container">
        <div class="row">
            <div class="col-xl-9 col-lg-8 col-md-7">
                <div class="section-title text-white">
                    <h1 style="color: #0b2e13">Ваша реклама</h1>
                </div>
                <div class="section-content">
                    <div class="my-post pending">
                        <div class="left">
                            <span class="image">
                                <img src="http://gikmir.loc/storage/12/conversions/zIUvTW8GHKlDqUByoCzxRg-thumb.jpg" alt="name">
                            </span>
                        </div>
                        <div class="right">
                            <h2>Title</h2>
                            <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, at, consequuntur cumque, distinctio dolorem hic illum laboriosam neque officia quos ratione rem sapiente sequi similique sit soluta sunt unde voluptatem!</p>
                            <span class="status"></span>
                        </div>
                        <div class="actions">
                            <span>show</span>
                            <span>edit</span>
                            <span>delete</span>
                        </div>
                    </div>
                    <div class="my-post rejected">
                        <div class="left">
                            <span class="image">
                                <img src="http://gikmir.loc/storage/12/conversions/zIUvTW8GHKlDqUByoCzxRg-thumb.jpg" alt="name">
                            </span>
                        </div>
                        <div class="right">
                            <h2>Title</h2>
                            <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, at, consequuntur cumque, distinctio dolorem hic illum laboriosam neque officia quos ratione rem sapiente sequi similique sit soluta sunt unde voluptatem!</p>
                            <span class="status"></span>
                        </div>
                        <div class="actions">
                            <span>show</span>
                            <span>edit</span>
                            <span>delete</span>
                        </div>
                    </div>
                    <div class="my-post approved">
                        <div class="left">
                            <span class="image">
                                <img src="http://gikmir.loc/storage/12/conversions/zIUvTW8GHKlDqUByoCzxRg-thumb.jpg" alt="name">
                            </span>
                        </div>
                        <div class="right">
                            <h2>Title</h2>
                            <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, at, consequuntur cumque, distinctio dolorem hic illum laboriosam neque officia quos ratione rem sapiente sequi similique sit soluta sunt unde voluptatem!</p>
                            <span class="status"></span>
                        </div>
                        <div class="actions">
                            <span>show</span>
                            <span>edit</span>
                            <span>delete</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
